package com.example.cpidx.agent;

import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;

public class Agent {
    static private int accessCount;
    static private Transformer transformer;
    static private Instrumentation instrumentation;

    public static void premain(String args, Instrumentation instrumentation) {
        Agent.instrumentation = instrumentation;
        transformer = new Transformer();
        instrumentation.addTransformer(transformer, true);
    }

    static public void objectAccessed(Object theObject) {
        ++accessCount;
    }

    static public void transformClass(Class<?> cls) throws UnmodifiableClassException {
        transformer.setRetransClassName(cls.getName().replace('.', '/'));
        instrumentation.retransformClasses(cls);
    }

    public static int getAccessCount() {
        return accessCount;
    }
}
