package com.example.cpidx.sb;

import java.lang.instrument.UnmodifiableClassException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.ReflectionUtils;

import com.example.cpidx.agent.Agent;

@SpringBootApplication
public class SbApplication {

    public static void main(String[] args) throws UnmodifiableClassException, ClassNotFoundException {
        transformClassByName(
            "org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryAutoConfiguration");
		SpringApplication.run(SbApplication.class, args);
	}

    private static void transformClassByName(String className)
        throws ClassNotFoundException, UnmodifiableClassException {
        Class<?> cls = SbApplication.class.getClassLoader().loadClass(className);
        ReflectionUtils.doWithLocalMethods(cls, m -> {
        });
        Agent.transformClass(cls);
    }
}
