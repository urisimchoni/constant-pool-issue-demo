package com.example.cpidx.app;

import java.lang.instrument.UnmodifiableClassException;

import com.example.cpidx.agent.Agent;

public class Main {
    public static void main(String[] args) throws UnmodifiableClassException {
        Agent.transformClass(Bean.class);

        Bean bean = new Bean();
        bean.setAnInt(4);
        bean.setaString("Hello");
        if (Agent.getAccessCount() != 2) {
            throw new RuntimeException("Expected two bean accesses");
        }

        System.out.println("OK!");
    }
}
