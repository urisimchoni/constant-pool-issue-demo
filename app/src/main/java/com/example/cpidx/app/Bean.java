package com.example.cpidx.app;

public class Bean {
    private int anInt;
    private String aString;

    public int getAnInt() {
        return anInt;
    }

    @Autowired(required = false)
    public void setAnInt(int anInt) {
        this.anInt = anInt;
    }

    public String getaString() {
        return aString;
    }

    public void setaString(String aString) {
        this.aString = aString;
    }

    @Override
    public String toString() {
        return "Bean{" +
            "anInt=" + anInt +
            ", aString='" + aString + '\'' +
            '}';
    }
}
