A Demo for OpenJ9 Issue #13071
================================

To build:

```
./gradlew clean build shadowJar
```

To run:

```
java -javaagent:./agent/build/libs/agent-1.0-SNAPSHOT-all.jar -jar sb/build/libs/sb-1.0-SNAPSHOT.jar
```

With HotSpot-VM-based OpenJDK, the Spring-boot application initializes and start listening for connections.
With OpenJ9-VM-based OpenJDK, an exception is throw.

Project Structure:

* agent - a minimal Java agent that transforms classes
* sb - a skeletal Spring-boot application which also invokes the agent
* app - a sample app to demonstrate that the agent works - not required for
  demonstrating the issue.

